# Optymalna budowa

## Wprowadzenie

Podczas robót budowlanych zachodzi potrzeba rozwiązania problemu optymalizacyjnego dotyczącego przygotowania prętów do konstrukcji stalowych. Projekt budowy określa liczbę i długości potrzebnych odcinków prętów. W cenniku sprzedawcy są długości prętów i ich ceny. Rozwiązanie wskazuje, ile prętów jakiej długości należy kupić i jak podzielić je na odcinki. Pręt dzielimy na odcinki, tnąc go. Niewykorzystaną część pręta, jeśli taka zostanie, odrzucamy. Łączenie prętów nie jest możliwe.

Wśród strategii wyboru rozwiązania są:
- Strategia minimalistyczna - Działa zachłannie. Dopóki problem nie jest rozwiązany, z cennika wybiera najkrótszy pręt, w którym mieści się najdłuższy brakujący odcinek. Następnie rozważa brakujące odcinki w kolejności od najdłuższych. Jeśli rozważany odcinek mieści się w części pręta, która jeszcze pozostała, jest od niej odcinany. To, co zostanie z pręta, po rozważeniu ostatniego odcinka, jest odpadem.
- Strategia maksymalistyczna - Działa tak, jak strategia minimalistyczna, ale z cennika zawsze wybiera najdłuższy pręt.
- Strategia ekonomiczna - Znajduje jedno z, być może wielu, rozwiązań minimalizujących koszt zakupu prętów.
- Strategia ekologiczna - Znajduje jedno z, być może wielu, rozwiązań minimalizujących długość odpadów.

Program:
- czyta ze standardowego wejścia cennik prętów, opis projektu i nazwę strategii,
- za pomocą wskazanej strategii rozwiązuje problem optymalizacyjny,
- pisze na standardowe wyjście rozwiązanie, określając jego jakość, kupione pręty i sposób ich podziału.

## Użycie

Dane programu to ciąg wierszy. We wszystkich, z wyjątkiem ostatniego, są liczby całkowite w zapisie dziesiętnym. Między każdą parą liczb sąsiadujących w wierszu jest jedna spacja. W pierwszym wierszu danych jest długość cennika C. W C kolejnych wierszach są pary dodatnich liczb całkowitych. Pierwsza z tych liczb określa długość pręta a druga to jego cena. Pary są uporządkowane rosnąco po długości pręta. Po cenniku jest wiersz z długością projektu P. W następnym wierszu jest P dodatnich liczb całkowitych, uporządkowanych niemalejąco. Liczby określają długości odcinków, potrzebnych do realizacji projektu. W ostatnim wierszu danych, po projekcie, jest słowo minimalistyczna, maksymalistyczna, ekonomiczna lub ekologiczna, będące nazwą wybranej strategii.



