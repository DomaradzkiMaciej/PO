package solution;

import java.util.ArrayList;

public class Ecological_strategy extends Economic_and_ecological_strategy {

    public Ecological_strategy(Rod[] array_of_available_rods, int[] array_of_necessary_sections){
        super( array_of_available_rods, array_of_necessary_sections);
    }

    @Override
    public Solution Solve(){

        Solution solution;
        solution = Solve( "Ecological_strategy", array_of_necessary_sections, array_of_necessary_sections.length,
                   new Solution( 0, 0, new ArrayList<>()));

        return solution;
    }
}
