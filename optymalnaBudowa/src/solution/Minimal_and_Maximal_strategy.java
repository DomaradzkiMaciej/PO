package solution;

public abstract class Minimal_and_Maximal_strategy extends Strategy {

    public Minimal_and_Maximal_strategy(Rod[] array_of_available_rods, int[] array_of_necessary_sections){
        super(array_of_available_rods, array_of_necessary_sections);
    }

    protected int The_longest_missing_section(){

        for( int i=array_of_necessary_sections.length-1; i>=0; i--){
            if( array_of_necessary_sections[i]!=0) {
                return array_of_necessary_sections[i];
            }
    }

        return 0;
    }

    protected int The_longest_missing_section_shorter_or_equal_used_rod( Rod used_rod){

        int the_longest_missing_section_shorter_or_equal_used_rod;

        for( int i=array_of_necessary_sections.length-1; i>=0; i--){
            if( array_of_necessary_sections[i]!=0 && array_of_necessary_sections[i] <= used_rod.length){
                the_longest_missing_section_shorter_or_equal_used_rod = array_of_necessary_sections[i];
                array_of_necessary_sections[i] = 0;
                return the_longest_missing_section_shorter_or_equal_used_rod;
            }
        }

        return 0;
    }
}
