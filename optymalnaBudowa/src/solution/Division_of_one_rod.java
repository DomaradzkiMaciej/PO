package solution;

import java.util.ArrayList;

public class Division_of_one_rod {

    private int[] used_sections;
    private int length_of_used_sections;

    private Division_of_one_rod( int size_of_array_of_used_sections){
        this.used_sections = new int[ size_of_array_of_used_sections];
        this.length_of_used_sections = 0;
    }

    private Division_of_one_rod( Division_of_one_rod division_of_one_rod){
        this.used_sections = division_of_one_rod.used_sections.clone();
        this.length_of_used_sections = division_of_one_rod.length_of_used_sections;
    }

    public int getLength_of_used_sections(){
        return length_of_used_sections;
    }

    public static ArrayList<Division_of_one_rod> optimal_cuttings_of_one_rod( Rod rod, int[] array_of_necessary_sections){
        //Funkcja zwraca ArrayList ze wszystkimi podziałami preta na potrzebne odcinki w taki sposob, ze nie da sie
        //wziac z tego preta zadnego odcinka wiecej.

        ArrayList<Division_of_one_rod> divisions_of_one_rod = new ArrayList<>();
        optimal_cuttings_of_one_rod( rod, array_of_necessary_sections, divisions_of_one_rod, new Division_of_one_rod( array_of_necessary_sections.length), 0, 0);

        return divisions_of_one_rod;
    }

    private static void optimal_cuttings_of_one_rod( Rod rod, int[] array_of_necessary_sections,
                                  ArrayList<Division_of_one_rod> divisions_of_one_rod, Division_of_one_rod division_of_one_rod, int length, int i){

        if( length > rod.length){
            return;
        }

        if( i >= array_of_necessary_sections.length){
            division_of_one_rod.length_of_used_sections = length;

            if( the_smallest_unused_section( array_of_necessary_sections, division_of_one_rod) == 0 ||
                    length + the_smallest_unused_section( array_of_necessary_sections, division_of_one_rod) > rod.length){
                divisions_of_one_rod.add( division_of_one_rod);
            }

            return;
        }

        Division_of_one_rod division_of_one_rod1 = new Division_of_one_rod( division_of_one_rod);
        division_of_one_rod1.used_sections[i] = array_of_necessary_sections[i];
        Division_of_one_rod division_of_one_rod2 = new Division_of_one_rod( division_of_one_rod);
        division_of_one_rod2.used_sections[i] = 0;

        if( 0 == array_of_necessary_sections[i]) {
            optimal_cuttings_of_one_rod( rod, array_of_necessary_sections, divisions_of_one_rod,
                    division_of_one_rod2, length, i+1);
        }
        else {
            optimal_cuttings_of_one_rod(rod, array_of_necessary_sections, divisions_of_one_rod,
                    division_of_one_rod1, length + array_of_necessary_sections[i], i + 1);
            optimal_cuttings_of_one_rod(rod, array_of_necessary_sections, divisions_of_one_rod,
                    division_of_one_rod2, length, i + 1);
        }
    }

    private static int the_smallest_unused_section( int[] array_of_necessary_sections, Division_of_one_rod division_of_one_rod){

        for (int i=0; i < array_of_necessary_sections.length; i++){
            if( array_of_necessary_sections[i] > 0 && division_of_one_rod.used_sections[i] == 0){
                return array_of_necessary_sections[i];
            }
        }

        return 0;
    }

    public String toString( int length_of_array){

        StringBuffer division_of_one_rod = new StringBuffer( "" + length_of_array);

        for ( int i: used_sections){
            if (i!=0){
                division_of_one_rod.append(" ").append(i);
            }
        }

        return  division_of_one_rod.toString();
    }

    public int Number_of_used_sections(){

        int number_of_used_sections = 0;

        for ( int i: used_sections){
            if (i!=0){
                number_of_used_sections++;
            }
        }

        return number_of_used_sections;
    }

    public int [] get_array_of_necessary_sections_without_used_sections( int[] array_of_necessary_sections){

        int[] new_array_of_necessary_sections = new int[ array_of_necessary_sections.length];

        for( int i=0; i < array_of_necessary_sections.length; i++){
            if( used_sections[i] == 0){
                new_array_of_necessary_sections[i] = array_of_necessary_sections[i];
            }
        }

        return new_array_of_necessary_sections;
    }

    public boolean isEmpty(){

        boolean isempty = true;

        for ( int i: used_sections){
            if (i!=0){
                isempty = false;
            }
        }

        return isempty;
    }
}
