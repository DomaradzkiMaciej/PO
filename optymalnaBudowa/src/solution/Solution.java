package solution;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public int purchase_price;
    public int length_of_wastes;
    public List<String> division_of_rods;

    public Solution( int purchase_price, int length_of_wastes, List<String> division_of_rods){
        this.purchase_price = purchase_price;
        this.length_of_wastes = length_of_wastes;
        this.division_of_rods = division_of_rods;
    }

    public Solution( Solution solution){
        this.purchase_price = solution.purchase_price;
        this.length_of_wastes = solution.length_of_wastes;
        this.division_of_rods = new ArrayList<>( solution.division_of_rods);
    }

    public void Write(){

        System.out.println( purchase_price);
        System.out.println( length_of_wastes);

        for( String division_of_a_rod : division_of_rods){
            System.out.println( division_of_a_rod);
        }
    }
}
