package solution;

public abstract class Strategy {

    protected Rod[] array_of_available_rods;
    protected int [] array_of_necessary_sections;

    public Strategy(Rod[] array_of_available_rods, int[] array_of_necessary_sections){
        this.array_of_available_rods = array_of_available_rods;
        this.array_of_necessary_sections = array_of_necessary_sections;
    }

    public abstract Solution Solve();
}
