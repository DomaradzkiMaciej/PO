package solution;

import java.util.ArrayList;
import java.util.List;

public class Maximal_strategy extends Minimal_and_Maximal_strategy {

    public Maximal_strategy(Rod[] array_of_available_rods, int[] array_of_necessary_sections){
        super(array_of_available_rods, array_of_necessary_sections);
    }

    @Override
    public Solution Solve(){

        int the_longest_missing_section_shorter_or_equal_used_rod;
        int number_of_missing_section  = array_of_necessary_sections.length;
        int purchase_price = 0;
        int length_of_wastes = 0;

        Rod using_rod = The_longest_available_rod();
        StringBuilder division_of_a_rod = new StringBuilder( "" + using_rod.length);
        List<String> division_of_rods = new ArrayList<>();

        purchase_price += using_rod.price;

        while( number_of_missing_section > 0){

            the_longest_missing_section_shorter_or_equal_used_rod = The_longest_missing_section_shorter_or_equal_used_rod( using_rod);

            if( the_longest_missing_section_shorter_or_equal_used_rod > 0){
                using_rod.length -= the_longest_missing_section_shorter_or_equal_used_rod;
                division_of_a_rod.append(" ").append( the_longest_missing_section_shorter_or_equal_used_rod);
                number_of_missing_section--;
            }
            else{
                length_of_wastes += using_rod.length;
                division_of_rods.add( division_of_a_rod.toString());
                using_rod = The_longest_available_rod();
                division_of_a_rod = new StringBuilder( "" + using_rod.length);
                purchase_price += using_rod.price;
            }
        }

        length_of_wastes += using_rod.length;
        division_of_rods.add( division_of_a_rod.toString());

        return new Solution( purchase_price, length_of_wastes, division_of_rods);
    }

    private Rod The_longest_available_rod(){

        return new Rod( array_of_available_rods[ array_of_available_rods.length-1]);
    }
}
