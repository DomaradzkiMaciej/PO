package solution;

import java.util.ArrayList;

public abstract class Economic_and_ecological_strategy extends Strategy {

    public Economic_and_ecological_strategy(Rod[] array_of_available_rods, int[] array_of_necessary_sections){
        super(array_of_available_rods, array_of_necessary_sections);
    }

    protected Solution Solve(String which_strategy, int[] array_of_necessary_sections, int number_of_needed_sections, Solution solution){

        if( 0 == number_of_needed_sections){
            return solution;
        }

        Solution new_solution;
        ArrayList<Solution> list_of_solutions;
        ArrayList<ArrayList<Solution>> list_of_list_of_solutions = new ArrayList<>();
        ArrayList<Division_of_one_rod> divisions_of_one_rod;

        for( int i=0; i < array_of_available_rods.length; i++){
            list_of_solutions = new ArrayList<>();
            divisions_of_one_rod = Division_of_one_rod.optimal_cuttings_of_one_rod( array_of_available_rods[i], array_of_necessary_sections);


            if( divisions_of_one_rod.size() != 0 && !divisions_of_one_rod.get(0).isEmpty()){
                for ( Division_of_one_rod division_of_one_rod: divisions_of_one_rod) {
                    new_solution = new Solution( solution);
                    new_solution.purchase_price += array_of_available_rods[i].price;
                    new_solution.length_of_wastes += array_of_available_rods[i].length - division_of_one_rod.getLength_of_used_sections();
                    new_solution.division_of_rods.add( division_of_one_rod.toString( array_of_available_rods[i].length));

                    list_of_solutions.add( Solve(which_strategy, division_of_one_rod.get_array_of_necessary_sections_without_used_sections(array_of_necessary_sections),
                            number_of_needed_sections - division_of_one_rod.Number_of_used_sections(), new_solution));
                }
                list_of_list_of_solutions.add( list_of_solutions);
            }
        }

        Solution the_best_solution = list_of_list_of_solutions.get(0).get(0);

        if( which_strategy.equals( "Economic_strategy")) {
            for( ArrayList<Solution> list_of_solutions1 : list_of_list_of_solutions) {
                for ( Solution j : list_of_solutions1){
                    if (the_best_solution == null || (j != null && the_best_solution.purchase_price > j.purchase_price)) {
                        the_best_solution = j;
                    }
                }
            }
        }
        else{
            for( ArrayList<Solution> list_of_solutions1 : list_of_list_of_solutions) {
                for ( Solution j : list_of_solutions1){
                    if (the_best_solution == null || (j != null && the_best_solution.length_of_wastes > j.length_of_wastes)) {
                        the_best_solution = j;
                    }
                }
            }
        }

        return the_best_solution;
    }
}
