package solution;

public class Rod {

    public int length;
    public int price;

    public Rod( int length, int price){
        this.length = length;
        this.price = price;
    }

    public Rod( Rod rod){
        this.length = rod.length;
        this.price = rod.price;
    }
}
