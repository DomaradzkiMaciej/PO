package input;

import java.util.Scanner;
import solution.Rod;

public class Input {

    private Rod[] array_of_available_rods;
    private int[] array_of_necessary_sections;
    private String which_strategy;

    private Input(){}

    public Rod[] getArray_of_available_rods(){
        return array_of_available_rods;
    }

    public int[] getArray_of_necessary_sections(){
        return array_of_necessary_sections;
    }

    public String getWhich_strategy(){
        return which_strategy;
    }

    public static Input Read(){

        Scanner scan = new Scanner(System.in);
        Input input = new Input();

        input.array_of_available_rods = read_available_rods( scan);
        input.array_of_necessary_sections = read_necessary_sections( scan);
        input.which_strategy = scan.nextLine();

        scan.close();

        return input;
    }

    private static Rod[] read_available_rods(Scanner scan){

        int number_of_available_rods;
        Rod[] array_of_available_rods;
        String line;
        String[] numbers_in_line;

        number_of_available_rods = scan.nextInt();
        scan.nextLine();
        array_of_available_rods = new Rod[ number_of_available_rods];

        for( int i=0; i < number_of_available_rods; i++){
            line = scan.nextLine();
            numbers_in_line = line.split(" ");

            array_of_available_rods[i] = new Rod( Integer.parseInt( numbers_in_line[0]), Integer.parseInt( numbers_in_line[1]));
        }

        return array_of_available_rods;
    }

    private static int[] read_necessary_sections( Scanner scan){

        int number_of_necessary_sections;
        int[] array_of_necessary_sections;
        String line;
        String[] numbers_in_line;

        number_of_necessary_sections = scan.nextInt();
        scan.nextLine();
        array_of_necessary_sections = new int[ number_of_necessary_sections];

        line = scan.nextLine();
        numbers_in_line = line.split(" ");

        for( int i = 0; i < number_of_necessary_sections; i++){
            array_of_necessary_sections[i] = Integer.parseInt( numbers_in_line[i]);
        }

        return array_of_necessary_sections;
    }
}
