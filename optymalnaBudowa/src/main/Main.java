package main;

import input.Input;
import solution.*;

public class Main {

    public static void main(String[] args) {

        Input input = Input.Read();
        Strategy strategy;

        if( input.getArray_of_necessary_sections().length == 0 && input.getArray_of_available_rods().length == 0){
            System.out.println("0");
            System.out.println("0");

            return;
        }

        switch ( input.getWhich_strategy()){
            case "minimalistyczna":
                strategy = new Minimal_strategy( input.getArray_of_available_rods(), input.getArray_of_necessary_sections());
                break;

            case "maksymalistyczna":
                strategy = new Maximal_strategy( input.getArray_of_available_rods(), input.getArray_of_necessary_sections());
                break;

            case "ekonomiczna":
                strategy = new Economic_strategy( input.getArray_of_available_rods(), input.getArray_of_necessary_sections());
                break;

            case "ekologiczna":
                strategy = new Ecological_strategy( input.getArray_of_available_rods(), input.getArray_of_necessary_sections());
                break;

            default:
                System.out.println("ERROR");
                return;
        }

        Solution solution = strategy.Solve();
        solution.Write();
    }
}
